# 协议专栏特别福利 | 答疑解惑第二期

2018-08-22 刘超

![](https://static001.geekbang.org/resource/image/7e/34/7e8f9003ecce8b7825c4ed5193310334.jpg)  

### 协议专栏特别福利 | 答疑解惑第二期

你好，我是刘超。

第二期答疑涵盖第 3 讲至第 6 讲的内容。我依旧对课后思考题和留言中比较有代表性的问题作出回答。你可以点击文章名，回到对应的章节复习，也可以继续在留言区写下你的疑问，我会持续不断地解答。希望对你有帮助。

## [《第 3 讲 | ifconfig：最熟悉又陌生的命令行》](/articles/03.html)

### 课后思考题

你知道 net-tools 和 iproute2 的 “历史” 故事吗？

![](https://static001.geekbang.org/resource/image/02/ba/02ae5ca5ab1c87bf5fea29196725c0ba.png)

这个问题的答案，盖同学已经写的比较全面了。具体的对比，我这里推荐一篇文章 [https://linoxide.com/linux-command/use-ip-command-linux/](https://linoxide.com/linux-command/use-ip-command-linux/)，感兴趣的话可以看看。

### 留言问题

1.A、B、C 类地址的有效地址范围是多少？

![](https://static001.geekbang.org/resource/image/eb/c5/ebf13d11cb0bc03d520c6cc4796ee8c5.png)

我在写的时候，没有考虑这么严谨，平时使用地址的时候，也是看个大概的范围。所以这里再回答一下。

A 类 IP 的地址第一个字段范围是 0～127，但是由于全 0 和全 1 的地址用作特殊用途，实际可指派的范围是 1～126。所以我仔细查了一下，如果较真的话，你在答考试题的时候可以说，A 类地址范围和 A 类有效地址范围。

2\. 网络号、IP 地址、子网掩码和广播地址的先后关系是什么？

![](https://static001.geekbang.org/resource/image/25/56/254f28a3623ca8b4ee368ac02f0cf656.png)

当在一个数据中心或者一个办公室规划一个网络的时候，首先是网络管理员规划网段，一般是根据将来要容纳的机器数量来规划，一旦定了，以后就不好变了。

假如你在一个小公司里，总共就没几台机器，对于私有地址，一般选择 192.168.0.0/24 就可以了。

这个时候先有的是网络号。192.168.0 就是网络号。有了网络号，子网掩码同时也就有了，就是前面都是网络号的是 1，其他的是 0，广播地址也有了，除了网络号之外都是 1。

当规划完网络的时候，一般这个网络里面的第一个、第二个地址被默认网关 DHCP 服务器占用，你自己创建的机器，只要和其他的不冲突就可以了，当然你也可以让 DHCP 服务自动配置。

规划网络原来都是网络管理员的事情。有了公有云之后，一般有个概念虚拟网络（VPC），鼠标一点就能创建一个网络，网络完全软件化了，任何人都可以做网络规划。

3\. 组播和广播的意义和原理是什么？

![](https://static001.geekbang.org/resource/image/92/f1/92654ed9549bba6c4e609e256ba083f1.png)

C 类地址的主机号 8 位，去掉 0 和 255，就只有 254 个了。

在《TCP/IP 详解》这本书里面，有两章讲了广播、多播以及 IGMP。广播和组播分为两个层面，其中 MAC 层有广播和组播对应的地址，IP 层也有自己的广播地址和组播地址。

广播相对比较简单，MAC 层的广播为 ff:ff:ff:ff:ff:ff，IP 层指向子网的广播地址为主机号为全 1 且有特定子网号的地址。

组播复杂一些，MAC 层中，当地址中最高字节的最低位设置为 1 时，表示该地址是一个组播地址，用十六进制可表示为 01:00:00:00:00:00。IP 层中，组播地址为 D 类 IP 地址，当 IP 地址为组播地址的时候，有一个算法可以计算出对应的 MAC 层地址。

多播进程将目的 IP 地址指明为多播地址，设备驱动程序将它转换为相应的以太网地址，然后把数据发送出去。这些接收进程必须通知它们的 IP 层，它们想接收的发给定多播地址的数据报，并且设备驱动程序必须能够接收这些多播帧。这个过程就是 “加入一个多播组”。

当多播跨越路由器的时候，需要通过 IGMP 协议告诉多播路由器，多播数据包应该如何转发。

4.MTU 1500 的具体含义是什么？

![](https://static001.geekbang.org/resource/image/0b/3d/0b43a6d8c1b77daef705e6b08f40a43d.png)

MTU（Maximum Transmission Unit，最大传输单元）是二层的一个定义。以以太网为例，MTU 为 1500 个 Byte，前面有 6 个 Byte 的目标 MAC 地址，6 个 Byte 的源 MAC 地址，2 个 Byte 的类型，后面有 4 个 Byte 的 CRC 校验，共 1518 个 Byte。

在 IP 层，一个 IP 数据报在以太网中传输，如果它的长度大于该 MTU 值，就要进行分片传输。如果不允许分片 DF，就会发送 ICMP 包，这个在 [ICMP](https://time.geekbang.org/column/article/8445) 那一节讲过。

在 TCP 层有个 MSS（Maximum Segment Size，最大分段大小），它等于 MTU 减去 IP 头，再减去 TCP 头。即在不分片的情况下，TCP 里面放的最大内容。

在 HTTP 层看来，它的 body 没有限制，而且在应用层看来，下层的 TCP 是一个流，可以一直发送，但其实是会被分成一个个段的。

## [《第 4 讲 | DHCP 与 PXE：IP 是怎么来的，又是怎么没的》](/articles/04.html)

### 课后思考题

PXE 协议可以用来安装操作系统，但是如果每次重启都安装操作系统，就会很麻烦。你知道如何使得第一次安装操作系统，后面就正常启动吗？

![](https://static001.geekbang.org/resource/image/8d/c2/8db1eca548b4eaacfd9fbbb544a467c2.png)

一般如果咱们手动安装一台电脑的时候，都是有启动顺序的，如果改为硬盘启动，就没有问题了。

![](https://static001.geekbang.org/resource/image/05/cc/053273db99e6ef2458fcf48a534ac4cc.png)

好在服务器一般都提供 IPMI 接口，可以通过这个接口启动、重启、设置启动模式等等远程访问，这样就可以批量管理一大批机器。

![](https://static001.geekbang.org/resource/image/8f/09/8f423b7215d40642f8c712250b65b709.png)

这里提到 Cobbler，这是一个批量安装操作系统的工具。在 OpenStack 里面，还有一个 Ironic，也是用来管理裸机的。有兴趣的话可以研究一下。

### 留言问题

1\. 在 DHCP 网络里面，手动配置 IP 地址会冲突吗?

![](https://static001.geekbang.org/resource/image/6c/bc/6cd736774a770fe0ca5e6d42ad3425bc.png)

![](https://static001.geekbang.org/resource/image/62/d0/62b7fbe964db964060d501e3ef5252d0.png)

在一个 DHCP 网络里面，如果某一台机器手动配置了一个 IP 地址，并且在 DHCP 管理的网段里的话，DHCP 服务器是会将这个地址分配给其他机器的。一旦分配了，ARP 的时候，就会收到两个应答，IP 地址就冲突了。

当发生这种情况的时候，应该怎么办呢？DHCP 的过程虽然没有明确如何处理，但是 DHCP 的客户端和服务器都可以添加相应的机制来检测冲突。

如果由客户端来检测冲突，一般情况是，客户端在接受分配的 IP 之前，先发送一个 ARP，看是否有应答，有就说明冲突了，于是发送一个 DHCPDECLINE，放弃这个 IP 地址。

如果由服务器来检测冲突，DHCP 服务器会发送 ping，来看某个 IP 是否已经被使用。如果被使用了，它就不再将这个 IP 分配给其他的客户端了。

2.DHCP 的 Offer 和 ACK 应该是单播还是广播呢？

![](https://static001.geekbang.org/resource/image/2c/b0/2c3c71b16ffca3442ffb60c36de8dcb0.png)

![](https://static001.geekbang.org/resource/image/a6/68/a6745f6d648ce8819976fed6fdc82468.png)

没心没肺 回答的很正确。

这个我们来看 [DHCP 的 RFC](http://www.faqs.org/rfcs/rfc2131.html)，我截了个图放在这儿：

![](https://static001.geekbang.org/resource/image/5b/ba/5ba44ea86352594b78c30ce75cc123ba.jpg)

这里面说了几个问题。

正常情况下，一旦有了 IP 地址，DHCP Server 还是希望通过单播的方式发送 OFFER 和 ACK。但是不幸的是，有的客户端协议栈的实现，如果还没有配置 IP 地址，就使用单播。协议栈是不接收这个包的，因为 OFFER 和 ACK 的时候，IP 地址还没有配置到网卡上。

所以，一切取决于客户端的协议栈的能力，如果没配置好 IP，就不能接收单播的包，那就将 BROADCAST 设为 1，以广播的形式进行交互。

如果客户端的协议栈实现很厉害，即便是没有配置好 IP，仍然能够接受单播的包，那就将 BROADCAST 位设置为 0，就以单播的形式交互。

3.DHCP 如何解决内网安全问题?

![](https://static001.geekbang.org/resource/image/f7/8c/f7ff8bed68c94247a19772113d87e18c.png)

其实 DHCP 协议的设计是基于内网互信的基础来设计的，而且是基于 UDP 协议。但是这里面的确是有风险的。例如一个普通用户无意地或者恶意地安装一台 DHCP 服务器，发放一些错误或者冲突的配置；再如，有恶意的用户发出很多的 DHCP 请求，让 DHCP 服务器给他分配大量的 IP。

对于第一种情况，DHCP 服务器和二层网络都是由网管管理的，可以在交换机配置只有来自某个 DHCP 服务器的包才是可信的，其他全部丢弃。如果有 SDN，或者在云中，非法的 DHCP 包根本就拦截到虚拟机或者物理机的出口。

对于第二种情况，一方面进行监控，对 DHCP 报文进行限速，并且异常的端口可以关闭，一方面还是 SDN 或者在云中，除了被 SDN 管控端登记过的 IP 和 MAC 地址，其他的地址是不允许出现在虚拟机和物理机出口的，也就无法模拟大量的客户端。

## [《第 5 讲 | 从物理层到 MAC 层：如何在宿舍里自己组网玩联机游戏？》](/articles/05.html)

### 课后思考题

1\. 在二层中我们讲了 ARP 协议，即已知 IP 地址求 MAC；还有一种 RARP 协议，即已知 MAC 求 IP 的，你知道它可以用来干什么吗？

![](https://static001.geekbang.org/resource/image/e3/51/e3f91712382b9322beb02ec3d12ce851.png)

2\. 如果一个局域网里面有多个交换机，ARP 广播的模式会出现什么问题呢？

盖还说出了环路的问题。

![](https://static001.geekbang.org/resource/image/74/a8/74de5ae71807bd0ee3d95f44d7e82aa8.png)

没心没肺不但说明了问题，而且说明了方案。

![](https://static001.geekbang.org/resource/image/fa/a4/fa42fea4d9b4b8ed75008e3f1eadd7a4.png)

## [《第 6 讲 | 交换机与 VLAN：办公室太复杂，我要回学校》](/articles/06.html)

### 课后思考题

STP 协议能够很好地解决环路问题，但是也有它的缺点，你能举几个例子吗？

![](https://static001.geekbang.org/resource/image/b4/61/b4cb0842468ab9bb16916e9fb29ccb61.png)

![](https://static001.geekbang.org/resource/image/ee/a1/eebc423ca8a37fbc3e7dfc06f07e4ea1.png)

STP 的主要问题在于，当拓扑发生变化，新的配置消息要经过一定的时延才能传播到整个网络。

由于整个交换网络只有一棵生成树，在网络规模比较大的时候会导致较长的收敛时间，拓扑改变的影响面也较大，当链路被阻塞后将不承载任何流量，造成了极大带宽浪费。

## 留言问题

1\. 每台交换机的武力值是什么样的？

![](https://static001.geekbang.org/resource/image/ae/51/ae72cc32931fc72918dd2f5da0ea9e51.png)

![](https://static001.geekbang.org/resource/image/c2/d8/c24fe02c21f41449a3a896442bc3b4d8.png)

当一台交换机加入或者离开网络的时候，都会造成网络拓扑变化，这个时候检测到拓扑变化的网桥会通知根网桥，根网桥会通知所有的网桥拓扑发生变化。

网桥的 ID 是由网桥优先级和网桥 MAC 地址组成的，网桥 ID 最小的将成为网络中的根桥。默认配置下，网桥优先级都一样，默认优先级是 32768。这个时候 MAC 地址最小的网桥成为根网桥。但是如果你想设置某台为根网桥，就配置更小的优先级即可。

在优先级向量里面，Root Bridge ID 就是根网桥的 ID，Bridge ID 是网桥的 ID，Port ID 就是一个网桥上有多个端口，端口的 ID。

![](https://static001.geekbang.org/resource/image/c0/95/c04244bf0b91ae964871698e7811d695.png)

按照 RFC 的定义，ROOT PATH COST 是和出口带宽相关的，具体的数据如下：

![](https://static001.geekbang.org/resource/image/d6/b9/d606b2c9f636ea3bc5694ac95d51f7b9.jpg)

2\. 图中的 LAN 指的是什么？

![](https://static001.geekbang.org/resource/image/48/83/48efc105d54eff1ca6099e44df5c5a83.png)

在这一节中，这两张图引起了困惑。

![](https://static001.geekbang.org/resource/image/a5/85/a57a66bf97d326fd0b1355a0ba844e85.png)

![](https://static001.geekbang.org/resource/image/92/17/92450a4c1d7f261a3fbf06a12d621d17.png)

本来是为了讲二层的原理，做了个抽象的图，结果引起了大家的疑问，所以这里需要重新阐述一下。

首先，这里的 LAN1、LAN2、LAN 3 的说法的确不准确，因为通过网桥或者交换机连接，它们还是属于一个 LAN，其实这是三个物理网络，通过网桥或者交换机连接起来，形成一个二层的 LAN。

对于一层，也即物理层的设备，主要使用集线器（Hub），这里我们就用 Hub 将物理层连接起来。

于是我新画了两个图。

![](https://static001.geekbang.org/resource/image/ea/48/ea499064396aa5078b9fe9cd5f4fcc48.png)

![](https://static001.geekbang.org/resource/image/7d/e5/7d0ec3b857e7c85bba8aefc340a86ce5.png)

在这里，我用 Hub 将不同的机器连接在一起，形成一个物理段，而非 LAN。

3\. 在 MAC 地址已经学习的情况下，ARP 会广播到没有 IP 的物理段吗？

![](https://static001.geekbang.org/resource/image/1f/f4/1f0ae48921ea404705d93e8bcbf4daf4.png)

![](https://static001.geekbang.org/resource/image/0c/a8/0c5d8df850a2fff54c175789645bd7a8.png)

首先谢谢这两位同学指出错误，这里 ARP 的目标地址是广播的，所以无论是否进行地址学习，都会广播，而对于某个 MAC 的访问，在没有地址学习的时候，是转发到所有的端口的，学习之后，只会转发到有这个 MAC 的端口。

4.802.1Q VLAN 和 Port-based VLAN 有什么区别？

![](https://static001.geekbang.org/resource/image/98/4d/988673f41c561228c017e96435ea264d.png)

所谓 Port-based VLAN，一般只在一台交换机上起作用，比如一台交换机，10 个口，1、3、5、7、9 属于 VLAN 10。1 发出的包，只有 3、5、7、9 能够收到，但是从这些口转发出去的包头中，并不带 VLAN ID。

而 802.1Q 的 VLAN，出了交换机也起作用，也就是说，一旦打上某个 VLAN，则出去的包都带这个 VLAN，也需要链路上的交换机能够识别这个 VLAN，进行转发。
